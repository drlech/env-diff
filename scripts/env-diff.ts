import fs from 'fs';

type EnvEntries = [string, string][];

/**
 * Parses the contents of an env file into an array of key-value tuples.
 */
const parseEnv = (env: string): EnvEntries =>
    env
        // Env file may begin or end with whitespace.
        .trim()
        // Lines.
        .split('\n')
        // Lines may have excessive whitespace around them.
        .map((v) => v.trim())
        // Empty lines are sometimes inserted as separators.
        .filter(Boolean)
        // Comments.
        .filter((v) => !v.startsWith('#'))
        // Split on first occurrence of '='.
        .map((v) => v.split(/=(.*)/))
        // Above split adds final third element with empty string.
        // Remove it, and leave just variable and value.
        .map(([k, v]) => [k, v]);

/** Find all entries in `a` that are not present in `b`. */
const findMissingEntries = (a: EnvEntries, b: EnvEntries): string[] =>
    a.map(([k]) => k).filter((k) => !b.map(([k]) => k).includes(k));

/**
 * Extracts the paths of the env files from the command line arguments.
 *
 * The script accepts two arguments - env files to compare. If _both_
 * are not present this function will fall back to the defaults.
 */
const getFilesFromArgv = (argv: string[]): [string, string] => {
    const [, , a, b] = argv;
    if (!a || !b) {
        return ['.env', '.env.example'];
    }

    return [a, b];
};

/** Loads the contents of an env file. */
const loadEnv = (path: string): string => fs.readFileSync(path, 'utf8');

const toLines = (lines: string[]) => lines.join('\n');

export const testables = {
    parseEnv,
    findMissingEntries,
    getFilesFromArgv,
    toLines
};

const [env1FileName, env2FileName] = getFilesFromArgv(process.argv);
const env1 = parseEnv(loadEnv(env1FileName));
const env2 = parseEnv(loadEnv(env2FileName));

const missingFromEnv1 = findMissingEntries(env2, env1);
const missingFromEnv2 = findMissingEntries(env1, env2);

const emphasis = '\x1b[36m%s\x1b[0m';

console.log('\nMissing from', emphasis, env1FileName);
console.log(toLines(missingFromEnv1));

console.log('\nMissing from', emphasis, env2FileName);
console.log(toLines(missingFromEnv2));

console.log();
