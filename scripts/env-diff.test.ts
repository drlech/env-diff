import { describe, test, expect } from 'bun:test';
import { testables } from './env-diff';

const { parseEnv, findMissingEntries, getFilesFromArgv, toLines } = testables;

describe(parseEnv.name, () => {
    test('basic case', () => {
        const input = `
LOREM=ipsum
DOLOR=sit
`;

        expect(parseEnv(input)).toEqual([
            ['LOREM', 'ipsum'],
            ['DOLOR', 'sit']
        ]);
    });

    test('ignores excessive whitespace', () => {
        const input = `
    LOREM=ipsum
    DOLOR=sit
`;

        expect(parseEnv(input)).toEqual([
            ['LOREM', 'ipsum'],
            ['DOLOR', 'sit']
        ]);
    });

    test('ignores separating empty lines', () => {
        const input = `
LOREM=ipsum

DOLOR=sit
`;

        expect(parseEnv(input)).toEqual([
            ['LOREM', 'ipsum'],
            ['DOLOR', 'sit']
        ]);
    });

    test('ignores comments', () => {
        const input = `
# This is a comment
LOREM=ipsum
# Another comment
DOLOR=sit
`;

        expect(parseEnv(input)).toEqual([
            ['LOREM', 'ipsum'],
            ['DOLOR', 'sit']
        ]);
    });
});

describe(findMissingEntries.name, () => {
    test('basic case', () => {
        expect(
            findMissingEntries(
                [
                    ['LOREM', 'ipsum'],
                    ['DOLOR', 'sit']
                ],
                [['LOREM', 'ipsum']]
            )
        ).toEqual(['DOLOR']);
    });

    test('all keys match', () => {
        expect(
            findMissingEntries(
                [
                    ['LOREM', 'ipsum'],
                    ['DOLOR', 'sit']
                ],
                [
                    ['LOREM', 'ipsum'],
                    ['DOLOR', 'sit']
                ]
            )
        ).toEqual([]);
    });

    test('second env has more items', () => {
        expect(
            findMissingEntries(
                [['LOREM', 'ipsum']],
                [
                    ['LOREM', 'ipsum'],
                    ['DOLOR', 'sit']
                ]
            )
        ).toEqual([]);
    });
});

describe(getFilesFromArgv.name, () => {
    test('returns passed file names', () => {
        expect(getFilesFromArgv(['', '', 'a', 'b'])).toEqual(['a', 'b']);
    });

    test('missing params', () => {
        expect(getFilesFromArgv(['', ''])).toEqual(['.env', '.env.example']);
    });

    test('partial params', () => {
        expect(getFilesFromArgv(['', '', 'a'])).toEqual([
            '.env',
            '.env.example'
        ]);
    });
});

describe(toLines.name, () => {
    test('each entry becomes individual line', () => {
        expect(toLines(['foo', 'bar', 'baz'])).toEqual('foo\nbar\nbaz');
    });
});
